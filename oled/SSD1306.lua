-- ***************************************************************************
--       
--      
--
-- ***************************************************************************

-------------------------------------------------------------------
--WIFI联网及MQTT部分
-------------------------------------------------------------------
g_status = 0  --断网状态标志  0为重获IP 1为MQTT初始化 2为正常通信
--sent_status = 0 --发布成功状态标志
--recv_status = 0 --接收成功状态标志
oled_temp = 0
oled_humi = 0

BROKER = {
    addr      = "www.1650.org",
    port      = 1883,
    secure    = 0,

}
CLIENT = {
    id        = "oled", -- clientid
    lwtmsg    = "offline",  -- lwt:Last Will and Testament, "offline"-> client does not send keepalive packet.
    username  = "", -- username, empty string -> not auth
    password  = "", -- password, empty string -> not auth
    keepalive = 60, -- keep alive tim er
    qos       = 1,  -- 0 -> at most 1 time, 1 -> at least 1 time
    retain    = 0   -- retian the message in server to ensure other subscribe device will receive the message
}

--WIFI配置部分
wifi.setmode(wifi.STATION)
--创建station_cfg配置表
station_cfg={}
station_cfg.ssid="TP-media.hjc"
station_cfg.pwd="wealnetpass123"
wifi.sta.config(station_cfg)
wifi.sta.autoconnect(1) --路由器重启自动连接WIFI

--WIFI连接监视函数
function connect_to_wifi()
    if wifi.sta.getip() == nil then
        print("Connecting...")
    else
        print("Connected, IP is "..wifi.sta.getip())
        -- connected to wifi, and status change to bind_event (1)
        g_status = 1
    end
end


function bind_event()
    -- clientid = nodemcu, kepalive=60s, username='', password =''
    m = mqtt.Client(CLIENT.id, CLIENT.keepalive, CLIENT.username, CLIENT.password)

    
    --注册事件回调函数  如果连接则打印
    m:on("connect", function(client) print ("1650.org was connected") end)
    --注册事件回调函数  如果被踢离线则打印
    m:on("offline", function(client) print ("offline") g_status = 1 end)
    --注册事件回调函数如果接收到消息则调用message_received函数
    m:on("message", message_received)
    m:lwt("/lwt", CLIENT.lwtmsg, CLIENT.qos, CLIENT.retian)
    -- for TLS: m:connect("192.168.1.128", secure-port, 1)
    m:connect(BROKER.addr, BROKER.port, BROKER.secure,
        function(client)

            --一次性订阅 oled状态 温度 湿度 主题
            value = m:subscribe({["oled"]=0,["sensor0_temp"]=0,["sensor0_humi"]=0}, function(conn) print("All done!") end)
            --发布成功返回true 否则返回false
            if value == true then
            	print("Subscribe done!")
            	g_status = 2
            else
            	print("Connection lost , Start reset!")
            	g_status = 1
            end
           -- g_status = 2
        end,
        function(client, reason)
        	print("failed reason: "..reason) --连接失败则廷时2秒仍进入本循环
        	g_status = 1
 --       	tmr.delay(2000)
        end)
    
end

function loop_pub()
          
     d = m:publish("oled","OK",1,0, function(client) --[[print("sent")]] end)
     --发布成功返回true 否则返回false
     if d then
     --print("Publish success!")
     else
        print("Connection lost again !  Start reset !")
        g_status = 1
     end

end



------------------------------------------------------------------------------------------------
--OLED显示部分
------------------------------------------------------------------------------------------------


-- setup SPI and connect display
function init_spi_display()
    -- Hardware SPI CLK  = GPIO14
    -- Hardware SPI MOSI = GPIO13
    -- Hardware SPI MISO = GPIO12 (not used)
    -- Hardware SPI /CS  = GPIO15 (not used)
    -- CS, D/C, and RES can be assigned freely to available GPIOs
    local cs  = 8 -- GPIO15, pull-down 10k to GND
    local dc  = 4 -- GPIO2  小灯闪光的原因
    local res = 0 -- GPIO16

    spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 8, 8)
    -- we won't be using the HSPI /CS line, so disable it again
    gpio.mode(8, gpio.INPUT, gpio.PULLUP)

    disp = u8g2.ssd1306_128x64_noname(1, cs, dc, res)
end


function u8g2_prepare()
    disp:setFont(u8g2.font_9x18B_tr)
    disp:setFontRefHeightExtendedText()
    disp:setDrawColor(1)
    disp:setFontPosTop()
    disp:setFontDirection(0)
end


--MQTT的消息处理函数传递数据给OLED
function message_received(client, topic, data)

    if topic == "oled" then
        if data ~=  nil then
            oled_status = data
        end
    elseif topic == "sensor0_temp" then
    	if data ~= nil then
    		 oled_temp = data
    	end
	elseif topic == "sensor0_humi" then
		if data ~= nil then
			oled_humi = data
    	end
    end
    
end

--显示函数 如果获得三个数据则显示 否则跳过
function showStr()

	if (oled_status ~= "-") and (oled_temp ~= "-") and (oled_humi ~= "-") then

		disp:clearBuffer()
		u8g2_prepare()

	    disp:drawStr( 0, 0, "NET:" .. oled_status)
	    disp:drawStr( 0, 15, "Temp:" .. oled_temp)
	   	disp:drawStr( 0, 30, "Humi:" .. oled_humi)

	    disp:sendBuffer()
	    oled_temp = "-"
		oled_humi = "-"
		oled_status = "-"
		--print("next")
	end	

    
end

--*************************************************************************************************************************
--网络及MQTT部分定时器循环
--
--**************************************************************************************************************************


init_spi_display()

print("--- Starting Graphics Test ---")

oled_temp = "-"
oled_humi = "-"
oled_status = "-"

--网络状态定时器循环
tmr.alarm(1, 2000, tmr.ALARM_AUTO, function ()
    if g_status == 0 then
        connect_to_wifi()
    elseif g_status == 1 then
        bind_event()
    elseif g_status == 2 then
        loop_pub()
    end
end)

--显示函数定时器
tmr.alarm(3,500,tmr.ALARM_AUTO, showStr)




--[[
tmr.alarm(2,3000,tmr.ALARM_AUTO,function() 

	print(oled_status .. "|" .. oled_temp .. "|" ..oled_humi)

	end)]]