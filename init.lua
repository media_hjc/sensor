--[[
- @Author: xsu19
- @Date:   2016-04-02 11:46:00
--]]

g_dht11 = {pin = 5, temp = 0, humi = 0}
g_status = 0
BROKER = {
    addr      = "www.1650.org",
    port      = 1883,
    secure    = 0,

}
CLIENT = {
    id        = "nodemcu_1", -- clientid
    lwtmsg    = "offline",  -- lwt:Last Will and Testament, "offline"-> client does not send keepalive packet.
    username  = "", -- username, empty string -> not auth
    password  = "", -- password, empty string -> not auth
    keepalive = 60, -- keep alive tim er
    qos       = 0,  -- 0 -> at most 1 time, 1 -> at least 1 time
    retain    = 0   -- retian the message in server to ensure other subscribe device will receive the message
} 

--WIFI配置部分
wifi.setmode(wifi.STATION)
--创建station_cfg配置表
station_cfg={}
station_cfg.ssid="TP-media.hjc"
station_cfg.pwd="wealnetpass123"
wifi.sta.config(station_cfg)
wifi.sta.autoconnect(1) --路由器重启自动连接WIFI

--WIFI连接监视函数
function connect_to_wifi()
    if wifi.sta.getip() == nil then
        print("Connecting...")
    else
        print("Connected, IP is "..wifi.sta.getip())
        -- connected to wifi, and status change to bind_event (1)
        g_status = 1
    end
end


function bind_event()
    -- clientid = nodemcu, kepalive=60s, username='', password =''
    m = mqtt.Client(CLIENT.id, CLIENT.keepalive, CLIENT.username, CLIENT.password)

    
    --注册事件回调函数  如果连接则打印
    m:on("connect", function(client) print ("1650.org was connected") end)
    --注册事件回调函数  如果离线则打印
    m:on("offline", function(client) print ("offline") g_status = 1 end)
    --注册事件回调函数如果接收到消息则调用message_received函数
    m:on("message", message_received)

    -- for TLS: m:connect("192.168.1.128", secure-port, 1)
    m:connect(BROKER.addr, BROKER.port, BROKER.secure,
        function(client)
            m:publish("home/sensor/outside/temp","init",0,0, function(client) print("sent") end)
            m:publish("home/sensor/outside/humi","init",0,0, function(client) print("sent") end)
            print("publish done!")
            a = m:subscribe("home/sensor/outside/temp",0, function(conn) print("subscribe temp success") end)
            b = m:subscribe("home/sensor/outside/humi",0, function(conn) print("subscribe humi success") end)
            --订阅成功返回true 否则返回false
            if a and b then
            	print("Subscribe done!")
            	g_status = 2
            else
            	print("Connection lost , Start reset!")
            	g_status = 1
            end

           -- g_status = 2
        end,
        function(client, reason)
        	print("failed reason: "..reason) --连接失败则廷时2秒仍进入本循环
        	g_status = 1
 --       	tmr.delay(2000)
        end)
    m:lwt("home/sensor/lwt", CLIENT.lwtmsg, 1, 1)
end

function loop_pub()
    status, temp, humi, temp_dec, humi_dec = dht.read(g_dht11.pin)
    if status == dht.OK then
        --if (g_dht11.temp ~= temp and g_dht11.humi ~= humi) then
          c = m:publish("home/sensor/outside/temp",temp,0,0, function(client) --[[print("sent")]] end)
          d = m:publish("home/sensor/outside/humi",humi,0,0, function(client) --[[print("sent")]] end)
          --发布成功返回true 否则返回false
          if c and d then
           	--print("Publish success!")
          else
           	print("Connection lost again !  Start reset !")
           	g_status = 1
          end
          print("DHT Temperature:"..temp..";".."Humidity:"..humi)
          g_dht11.temp = temp
          g_dht11.humi = humi
        --end
    elseif status == dht.ERROR_CHECKSUM then
        print( "DHT Checksum error." )
    elseif status == dht.ERROR_TIMEOUT then
        print( "DHT timed out." )
    end
end

-- receive subscribe message and data
function message_received(client, topic, data)
    -- about client : it is a userdata , do not print it
    print(topic .. ":" )
    if data ~= nil then
        print(data)
    end
end

--定时器循环
tmr.alarm(1, 3000, tmr.ALARM_AUTO, function ()
    if g_status == 0 then
        connect_to_wifi()
    elseif g_status == 1 then
        bind_event()
    elseif g_status == 2 then
        loop_pub()
    end
end)

